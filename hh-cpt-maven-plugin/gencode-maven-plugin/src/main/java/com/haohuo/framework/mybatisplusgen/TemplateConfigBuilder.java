package com.haohuo.framework.mybatisplusgen;

import com.baomidou.mybatisplus.generator.config.TemplateConfig;

import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * Copyright 2021 wtfu.site Inc. All Rights Reserved.
 *
 * @author: 12302
 * @date: 2021-05-09
 */
public class TemplateConfigBuilder {

    private TemplateConfig templateConfig;
    private String contains;

    public TemplateConfigBuilder(TemplateConfig templateConfig,String contains){
        this.templateConfig = templateConfig;
        this.contains = contains;
    }

    public TemplateConfigBuilder addLast(Predicate<String> predicate,
                                         Consumer<TemplateConfig> consumer,
                                         Consumer<TemplateConfig> negate){
        if(predicate.test(contains))
            consumer.accept(templateConfig);
        else
            negate.accept(templateConfig);
        return this;
    }
}
