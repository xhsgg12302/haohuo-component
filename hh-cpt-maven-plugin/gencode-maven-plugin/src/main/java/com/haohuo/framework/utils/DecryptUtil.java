package com.haohuo.framework.utils;

import org.sonatype.plexus.components.cipher.DefaultPlexusCipher;
import org.sonatype.plexus.components.cipher.PlexusCipherException;
import org.sonatype.plexus.components.sec.dispatcher.SecUtil;
import org.sonatype.plexus.components.sec.dispatcher.model.SettingsSecurity;

/**
 * @author wiki
 */
public class DecryptUtil {

    private static DefaultPlexusCipher defaultPlexusCipher;
    static {
        try{
            defaultPlexusCipher = new DefaultPlexusCipher();
        }catch (Exception e){ throw new RuntimeException(e);}
    }

    public static boolean isEncrypt(String origin){
        return defaultPlexusCipher.isEncryptedString(origin);
    }

    public static String decodeMaster(){
        try{
            SettingsSecurity read = SecUtil.read(System.getProperty("user.home") + "/.m2/settings-security.xml", true);
            if(read == null){ return null;}
            String encodedMasterPassword = defaultPlexusCipher.decryptDecorated(read.getMaster(),"settings.security");
            return encodedMasterPassword;
        }catch (Exception e){ System.out.println(e.getMessage()); return null;}
    }

    public static String decodeSlaver(String origin, String master) {
        try {
            return defaultPlexusCipher.decryptDecorated(origin,master);
        } catch (PlexusCipherException e) {
            e.printStackTrace();
            return null;
        }
    }
}
