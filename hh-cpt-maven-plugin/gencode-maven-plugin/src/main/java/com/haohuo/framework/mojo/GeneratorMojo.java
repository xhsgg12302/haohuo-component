package com.haohuo.framework.mojo;

import com.haohuo.framework.mybatisplusgen.CodeGenerator;
import com.haohuo.framework.mybatisplusgen.PropertyUtil;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.plexus.util.StringUtils;

import java.util.Properties;

/**
 *
 * Copyright https://wtfu.site Inc. All Rights Reserved.
 *
 * @date 2021/9/6
 *                          @since  1.0
 *                          @author 12302
 *
 */
@Mojo(name = "product")
public class GeneratorMojo extends AbstractMojo {

    /**
     * url
     */
    @Parameter(property = "product.url", required = true)
    private String url;

    /**
     * username
     */
    @Parameter(property = "product.username", required = true)
    private String username;

    /**
     * password
     */
    @Parameter(property = "product.password", required = true)
    private String password;

    /**
     * output 输出路径
     */
    @Parameter(property = "product.output", defaultValue = "${user.dir}/output")
    private String output;

    /**
     * driver 默认使用 mysql 的驱动
     */
    @Parameter(property = "product.driver", defaultValue = "com.mysql.jdbc.Driver")
    private String driver;

    /**
     * author
     */
    @Parameter(property = "product.author", defaultValue = "haohuo")
    private String author;

    /**
     * companySuffix
     */
    @Parameter(property = "product.companySuffix", defaultValue = "com")
    private String companySuffix;

    /**
     * moduleName
     */
    @Parameter(property = "product.moduleName", defaultValue = "haohuo.framework")
    private String moduleName;

    /**
     * controllerPkgName
     */
    @Parameter(property = "product.controllerPkgName", defaultValue = "controller")
    private String controllerPkgName;

    /**
     * composePkgName
     */
    @Parameter(property = "product.composePkgName", defaultValue = "compose")
    private String composePkgName;

    /**
     * entityPkgName
     */
    @Parameter(property = "product.entityPkgName", defaultValue = "beans")
    private String entityPkgName;

    /**
     * mapperPkgName
     */
    @Parameter(property = "product.mapperPkgName", defaultValue = "mapper")
    private String mapperPkgName;

    /**
     * xmlPkgName
     */
    @Parameter(property = "product.xmlPkgName", defaultValue = "xml")
    private String xmlPkgName;

    /**
     * servicePkgName
     */
    @Parameter(property = "product.servicePkgName", defaultValue = "service")
    private String servicePkgName;

    /**
     * implPkgName
     */
    @Parameter(property = "product.implPkgName", defaultValue = "service.impl")
    private String implPkgName;

    /**
     * contains 表示 需要生成的文件类型，默认七种 (controller, entity, mapper, xml, service, impl, compose)
     * 多个逗号分隔。
     */
    @Parameter(property = "product.contains", defaultValue = "controller, entity, mapper, xml, service, impl, compose")
    private String contains;

    /**
     * tablePrefix 表示表前缀，这个在生成实体的时候会去掉表前缀。
     */
    @Parameter(property = "product.tablePrefix", defaultValue = "t_")
    private String tablePrefix;

    /**
     * tables 表示要对哪些生成文件。多个表逗号分隔。 默认全部表
     */
    @Parameter(property = "product.tables", defaultValue = ".*")
    private String tables;

    /**
     * 是否对实体文件加入swagger相关注解
     */
    @Parameter(property = "product.swagger2", defaultValue = "false")
    private Boolean swagger2;


    @Parameter(property = "product.superEntityColumns")
    private String[] superEntityColumns;

    /**
     * 查看此插件可配置参数
     */
    @Parameter(property = "product.info" ,defaultValue = "false")
    private Boolean info;


    @Override
    public void execute() throws MojoExecutionException {
        PropertyUtil.fillFieldToProp(this);
        if(info){
            PropertyUtil.printProps(getLog());
            return;
        }
        if(StringUtils.isEmpty(password)
                || StringUtils.isBlank(username)
                || StringUtils.isBlank(url)){getLog().info("skip module");return;}

        try{
            CodeGenerator.generate();
            getLog().info("execute gen goal done!");
        }catch (Exception e){
            getLog().error(e.getMessage());
            getLog().error("goal execute exception...,please check properties configuration\n");
        }
    }
}
