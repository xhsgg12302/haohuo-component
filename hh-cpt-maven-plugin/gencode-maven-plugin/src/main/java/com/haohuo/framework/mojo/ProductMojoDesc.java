package com.haohuo.framework.mojo;

import com.haohuo.framework.mybatisplusgen.PropertyUtil;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;

/**
 *
 * Copyright https://wtfu.site Inc. All Rights Reserved.
 * 
 * @date 2022/12/2
 *                          @since  1.0
 *                          @author 12302
 * 
 */
@Mojo(name = "product-desc")
public class ProductMojoDesc extends AbstractMojo {
    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        PropertyUtil.fillFieldToProp(this);
        PropertyUtil.printProps(getLog());
    }
}
