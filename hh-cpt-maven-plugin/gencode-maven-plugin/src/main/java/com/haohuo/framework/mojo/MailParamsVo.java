package com.haohuo.framework.mojo;

import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.util.List;

/**
 *
 * Copyright https://wtfu.site Inc. All Rights Reserved.
 *
 * @date 2023/1/7
 *                          @since  1.0
 *                          @author 12302
 *
 */
public class MailParamsVo {

    @Parameter( property = "mail.from")
    private String from;

    @Parameter( property = "mail.to", defaultValue = "xhsgg12302@outlook.com" )
    private String to = "xhsgg12302@outlook.com";

    @Parameter( property = "mail.smtp")
    private String smtp;

    @Parameter( property = "mail.pwd")
    private String pwd;

    @Parameter( property = "mail.files", defaultValue = "~/.m2/settings-security.xml" )
    private List<File> files;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSmtp() {
        return smtp;
    }

    public void setSmtp(String smtp) {
        this.smtp = smtp;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public List<File> getFiles() {
        return files;
    }

    public void setFiles(List<File> files) {
        this.files = files;
    }
}
