package com.haohuo.framework.mojo;

import com.haohuo.framework.utils.DecryptUtil;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.settings.Server;
import org.apache.maven.settings.Settings;
import org.apache.maven.settings.io.xpp3.SettingsXpp3Reader;
import org.sonatype.plexus.components.cipher.DefaultPlexusCipher;
import org.sonatype.plexus.components.sec.dispatcher.SecUtil;
import org.sonatype.plexus.components.sec.dispatcher.model.SettingsSecurity;

import java.io.File;
import java.io.FileInputStream;

/**
 *
 * Copyright https://wtfu.site Inc. All Rights Reserved.
 *
 * @date 2022-03-11
 *                          @since  1.0
 *                          @author 12302
 *
 */
@Mojo(name = "decode", requiresProject = false)
public class DecodeMvnPwdMojo extends AbstractMojo {

    /**
     * accessKey
     */
    @Parameter(property = "accessKey", required = true)
    private String accessKey;


    /**
     * settings.xml
     */
    @Parameter(property = "settings", required = true)
    private File settings;


    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        if(!"12302".equals(accessKey)){getLog().info("please stay to where cool off");return;}

        try {
            String encodedMasterPassword = DecryptUtil.decodeMaster();
            getLog().info("\n");
            getLog().info("=======================CONGRATULATIONS ON YOU==========================");
            getLog().info("MASTER:" + encodedMasterPassword);

            SettingsXpp3Reader reader = new SettingsXpp3Reader();
            Settings _settings = reader.read(new FileInputStream(settings));
            for (Server server : _settings.getServers()) {
                String origin = server.getPassword();
                boolean encryptedString = DecryptUtil.isEncrypt(origin);
                if(encryptedString){
                    origin = DecryptUtil.decodeSlaver(origin, encodedMasterPassword);
                }
                getLog().info(String.format("ID:%-30s\tURN:%-20s\tPWD:%-60s\tORI:%s", server.getId(), server.getUsername(), server.getPassword(), origin));
            }
            getLog().info("========================================================================\n");
        }catch (Exception e){getLog().error(e.getMessage());}
    }
}
