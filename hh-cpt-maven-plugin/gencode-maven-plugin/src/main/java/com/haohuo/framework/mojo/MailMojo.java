package com.haohuo.framework.mojo;

import com.haohuo.framework.email.MailTax;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

/**
 * Copyright 2021 wtfu.site Inc. All Rights Reserved.
 *
 * @author: 12302
 * @date: 2021-07-31
 */
@Mojo(name = "spawn")
public class MailMojo extends AbstractMojo {

    /**
     * The greeting to display.
     */
    @Parameter( property = "sayhi.greeting", defaultValue = "Hello World!" )
    private String greeting;

    /**
     * the params
     */
    @Parameter(name = "mailParamsVo")
    private MailParamsVo mailParamsVo;

    @Override
    public void execute() throws MojoExecutionException {
        MailTax.sendMailWithAttachFile(mailParamsVo);
    }


}
