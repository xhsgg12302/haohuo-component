package com.haohuo.framework.mybatisplusgen;

import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 12302
 */
public class ExFreemarkerTemplateEngine extends FreemarkerTemplateEngine {

    public ExFreemarkerTemplateEngine(){
        this.saveObject.add(PropertyUtil.getProp("output"));
    }

    private List saveObject = new ArrayList<String>();

    @Override
    public void writer(Map<String, Object> objectMap, String templatePath, String outputFile) throws Exception {
        super.writer(objectMap, templatePath, outputFile);
        saveObject.add(outputFile);
    }

    public void serialization(String file){
        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
            oos.writeObject(saveObject);
        } catch (Exception e) {  e.printStackTrace();}
    }

    public static List deserialization(String file){
        try(ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file))){
            return ArrayList.class.cast(objectInputStream.readObject());
        } catch (Exception e){ e.printStackTrace();}
        return null;
    }

    @Test
    public void test(){
        String file = System.getProperty("user.dir") + "/src/main/resources/serialization.object";
        List deserialization = deserialization(file);
        System.out.println(deserialization);
    }
}
