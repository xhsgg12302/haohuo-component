package com.haohuo.framework.mybatisplusgen;

import org.apache.maven.plugin.logging.Log;
import org.sonatype.plexus.components.cipher.DefaultPlexusCipher;
import org.sonatype.plexus.components.sec.dispatcher.SecUtil;
import org.sonatype.plexus.components.sec.dispatcher.model.SettingsSecurity;

import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Properties;

/**
 * 配置文件读取工具类
 */
public class PropertyUtil {

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * 另外一种方式
     * _12302_2019-06-24
     */
    private static Properties defaultProp,prop;
    static{
        defaultProp = new Properties();
        try {
            defaultProp.load(new InputStreamReader(PropertyUtil.class.getClassLoader().getResourceAsStream("default.properties"), "UTF-8"));
            prop = new Properties(defaultProp);
            //prop = new Properties();
        } catch (IOException e) { e.printStackTrace(); }
    }

    public static void update(Properties prop){
        PropertyUtil.prop.putAll(prop);
    }

    public static void printProps(Log log){
        Enumeration<?> enumeration = prop.propertyNames();
        System.out.println();
        log.info("================YOU CAN OVERRIDE FOLLOW PROPERTIES==================================");
        while(enumeration.hasMoreElements()){
            String temp = (String)enumeration.nextElement();
            log.info("|| " +temp + ": " + prop.getProperty(temp));
        }
        log.info("====================================================================================\n");
    }
    public static void fillFieldToProp(Object object){
        Field[] declaredFields = object.getClass().getDeclaredFields();
        Arrays.stream(declaredFields).forEach(it -> {
            try{
                it.setAccessible(true);
                prop.put(it.getName(),it.get(object));
            }catch (Exception e){ e.printStackTrace();}
        });
    }

    public static String decrypt(String origin){
       try{
           DefaultPlexusCipher defaultPlexusCipher = new DefaultPlexusCipher();
           boolean encryptedString = defaultPlexusCipher.isEncryptedString(origin);
           if(encryptedString){
               SettingsSecurity read = SecUtil.read(System.getProperty("user.home") + "/.m2/settings-security.xml", true);
               String encodedMasterPassword = defaultPlexusCipher.decryptDecorated(read.getMaster(),"settings.security");
               origin = defaultPlexusCipher.decryptDecorated(origin, encodedMasterPassword);
           }
           return origin;
       }catch (Exception e ){ e.printStackTrace(); return origin;}
    }

    public static String getProp(String key){
        return getProp(key, String.class);
    }

    public static <T> T getProp(String key, Class<? extends T> tClass){
        T value = null;
        if(key!=null){
            Object origin = prop.get(key);

            if(origin.getClass() == tClass){
                return tClass.cast(origin);
            }else {
                //throw new RuntimeException("please check the application.properties if exist <" + key + ">");
                System.err.println("please check the default.properties if exist <" + key + ">");
            }
        }
        return value;
    }

    public static void main(String[] args) {
        System.out.println(getProp("parkCode"));
    }
}
