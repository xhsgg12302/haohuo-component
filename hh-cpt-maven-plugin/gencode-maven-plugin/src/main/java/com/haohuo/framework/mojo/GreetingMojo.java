package com.haohuo.framework.mojo;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

/**
 * Copyright 2021 wtfu.site Inc. All Rights Reserved.
 *
 * @author: 12302
 * @date: 2021-07-31
 */
@Mojo(name = "sayhi")
public class GreetingMojo extends AbstractMojo {

    /**
     * The greeting to display.
     */
    @Parameter( property = "sayhi.greeting", defaultValue = "Hello World!" )
    private String greeting;

    @Override
    public void execute() throws MojoExecutionException {
        getLog().info("xhsgg12302@126.com : " + greeting);
    }


}
