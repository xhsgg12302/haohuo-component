package ${cfg.composeImport};

import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务组合compose
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Service
<#if kotlin>
open class ${table.serviceImplName} : ${superServiceImplClass}<${table.mapperName}, ${entity}>(), ${table.serviceName} {

}
<#else>
public class ${entity}Compose  extends BaseCompose {

}
</#if>

