<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${package.Mapper}.${table.mapperName}">

<#if enableCache>
    <!-- 开启二级缓存 -->
    <cache type="org.mybatis.caches.ehcache.LoggingEhcache"/>

</#if>
<#if baseResultMap>

    <!-- tableName:【${table.name}】 -->

    <!-- 通用查询映射结果 -->
    <resultMap id="BaseResultMap" type="${package.Entity}.${entity}">
        <#list table.fields as field>
            <#if field.keyFlag><#--生成主键排在第一位-->
        <id column="${field.name}" property="${field.propertyName}"/>
            </#if>
        </#list>
<#list table.commonFields as field><#--生成公共字段 -->
        <result column="${field.name}" property="${field.propertyName}"/>
</#list>
<#list table.fields as field>
    <#if !field.keyFlag><#--生成普通字段 -->
        <result column="${field.name}" property="${field.propertyName}"/>
    </#if>
</#list>
    </resultMap>

</#if>
<#if baseColumnList>

    <!-- 通用查询结果列 -->
    <sql id="Base_Column_List">
        <#list table.commonFields as field>
            ${field.columnName},
        </#list>
        <#-- ${table.fieldNames} -->
        <#assign _list = table.fieldNames?split(" ") >
        <#list _list?chunk(5) as row >
            <#list row as cell> ${cell} </#list>
        </#list>
    </sql>

    <!-- 通用查询条件 -->
    <sql id="Query_Column_List">
        <where>
        <#list table.fields as field>
            <#--<if test="cm.${field.propertyName} != null <#if field.columnType == "STRING" >and cm.${field.propertyName} != ''</#if>">
                AND ${field.columnName} = <#noparse>#{</#noparse>cm.${field.propertyName}<#noparse>}</#noparse>
            </if>-->
            <if test="cm.${field.propertyName} != null and cm.${field.propertyName} != ''">
                AND ${field.columnName} = <#noparse>#{</#noparse>cm.${field.propertyName}<#noparse>}</#noparse>
            </if>
        </#list>
        </where>
    </sql>

</#if>
</mapper>
