package ${package.Controller};


import org.springframework.web.bind.annotation.RequestMapping;

<#if restControllerStyle>
import org.springframework.web.bind.annotation.RestController;
<#else>
import org.springframework.stereotype.Controller;
</#if>
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.haohuo.mns.InAuthToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import com.alibaba.fastjson.JSONObject;

import com.haohuo.utils.ParmsException;
import com.haohuo.utils.RetCode;
import com.haohuo.utils.ComResult;

/**
 * <p>
 *     ${table.comment!} 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if restControllerStyle>
//@InAuthToken
@RestController
<#else>
@Controller
</#if>
@Api(tags = "${entity}", description = "${table.comment!}")
@RequestMapping("/API/${entity}")
public class ${table.controllerName} extends BaseController{

    //@Autowired
    //private ${entity}Compose ${entity}Compose;


    @PostMapping(value = "list")
    @ApiOperation(value = "根据组合字段查询${table.comment!}", notes = "" +
            "输入：\n" +
            "{\n" +
        <#list table.fields as field>
                "\"${field.propertyName}\":\"${field.comment} <#if field.customMap['NULL'] == "YES">Y<#else>N</#if>\"<#if field_has_next>,</#if>\n" +
        </#list>
            "}\n" +
            "")
    public Object list(@RequestBody JSONObject obj)throws ParmsException {
        //是否需要检查参数
        checkParms(obj, <#list table.fields as field>"${field.propertyName}"<#if field_has_next>,</#if></#list>)
        //TODO
        //return ${entity}Compose.list(obj);
        return null;
    }



    @PostMapping(value = "add")
    @ApiOperation(value = "增加${table.comment!}", notes = "" +
            "输入：\n" +
            "{\n" +
        <#list table.fields as field>
                "\"${field.propertyName}\":\"${field.comment} <#if field.customMap['NULL'] == "YES">Y<#else>N</#if>\"<#if field_has_next>,</#if>\n" +
        </#list>
            "}\n" +
            "")
    public Object add(@RequestBody JSONObject obj) throws ParmsException {
        //是否需要检查参数
        checkParms(obj, <#list table.fields as field>"${field.propertyName}"<#if field_has_next>,</#if></#list>)
        //TODO
        //return ${entity}Compose.add(obj);
        return null;
    }



    @PostMapping(value = "update")
    @ApiOperation(value = "根据主键值和当前对象修改数据库单条记录", notes = "" +
            "输入：\n" +
            "{\n" +
            <#list table.fields as field>
                "\"${field.propertyName}\":\"${field.comment} <#if field.customMap['NULL'] == "YES">Y<#else>N</#if>\"<#if field_has_next>,</#if>\n" +
            </#list>
            "}\n" +
            "")
    public ComResult update(@RequestBody JSONObject obj)throws ParmsException {
        //是否需要检查参数
        checkParms(obj, <#list table.fields as field>"${field.propertyName}"<#if field_has_next>,</#if></#list>)
        //TODO
        //return ${entity}Compose.update(obj);
        return null;
    }


    @PostMapping(value = "delete")
    @ApiOperation(value = "根据传入对象的属性值作为条件删除对象！特别注意：如果一个参数都没有，会全表删除-_-!", notes = "" +
            "输入：\n" +
            "{\n" +
            <#list table.fields as field>
                "\"${field.propertyName}\":\"${field.comment} <#if field.customMap['NULL'] == "YES">Y<#else>N</#if>\"<#if field_has_next>,</#if>\n" +
            </#list>
            "}\n" +
            "")
    public ComResult delete(@RequestBody JSONObject obj)throws ParmsException {
        //是否需要检查参数
        checkParms(obj, <#list table.fields as field>"${field.propertyName}"<#if field_has_next>,</#if></#list>)
        //TODO
        //return ${entity}Compose.delete(obj);
        return null;
    }

}
