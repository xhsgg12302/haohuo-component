package com.haohuo.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by bianzhifu on 16/8/3.
 */
public class DateUtil {

    public final static String FORMAT_DEFAULT = "yyyy-MM-dd HH:mm:ss";
    public final static String FORMAT_DEFAULT_yyyyMMddHHmmss = "yyyyMMddHHmmss";
    public final static String FORMAT_DEFAULT_DATE = "yyyy-MM-dd";


    public static int getUnixTimeNow() {
        int unixTime = (int) (System.currentTimeMillis() / 1000);
        return unixTime;
    }

    public static int getUnixTimeToday() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return (int) (cal.getTimeInMillis() / 1000);
    }

    public static String getToday() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        return simpleDateFormat.format(Calendar.getInstance().getTime());
    }

    public static String get_Today() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return simpleDateFormat.format(Calendar.getInstance().getTime());
    }

    public static String get_Yestaday() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        return simpleDateFormat.format(calendar.getTime());
    }

    public static String get_Tomorrow() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        return simpleDateFormat.format(calendar.getTime());
    }

    public static String get_NowMonth() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
        Calendar calendar = Calendar.getInstance();
        return simpleDateFormat.format(calendar.getTime());
    }

    public static String get_NowMonths() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        return simpleDateFormat.format(calendar.getTime());
    }

    public static String get_NextMonth() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.add(Calendar.MONTH, 1);
        return simpleDateFormat.format(calendar.getTime());
    }

    public static String get_NextYearToday() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, 1);
        return simpleDateFormat.format(calendar.getTime());
    }

    public static String get_NextMonthToday() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, 1);
        return simpleDateFormat.format(calendar.getTime());
    }

    public static String get_SomeDay(int days) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, days);
        return simpleDateFormat.format(calendar.getTime());
    }

    public static Date getNowDate() {
        Calendar calendar = Calendar.getInstance();
        return calendar.getTime();
    }

    public static Calendar getNowCalendar() {
        Calendar calendar = Calendar.getInstance();
        return calendar;
    }

    /**
     * 获取当前时间
     *
     * @return yyyy-MM-dd HH:mm:ss
     */
    public static String getNowTimeStr() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return simpleDateFormat.format(Calendar.getInstance().getTime());
    }

    public static String getNowTimeStr(String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(Calendar.getInstance().getTime());
    }


    /**
     * 格式化时间 format yyyy-MM-dd HH:mm:ss
     */
    public static String formatDate(Date date, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date);
    }

    public static String formatNowDate(String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(Calendar.getInstance().getTime());
    }

    public static Date formatDate(String date, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        try {
            return simpleDateFormat.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    public static Calendar parseCalandar(String date, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        try {
            Date temp = simpleDateFormat.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(temp);
            return calendar;
        } catch (ParseException e) {
        }
        return null;
    }

    public static String formatCalandar(Calendar date, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date.getTime());
    }

    // 取今天的前后几天
    public static String getOffsetDate(int int2) {
        Calendar rightNow = Calendar.getInstance();
        SimpleDateFormat sim = new SimpleDateFormat("yyyy-MM-dd");
        rightNow.add(Calendar.DAY_OF_MONTH, int2);
        // 进行时间转换
        String date = sim.format(rightNow.getTime());
        return date;
    }

    /**
     * [获取两个日期之间的天数差]<br>
     *
     * @param beginDate 开始日
     * @param endDate   结束日
     * @return 天数差
     * @throws ParseException
     * @throws Exception      异常
     */
    public static int daysBetween(String beginDate, String endDate) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.setTime(sdf.parse(beginDate.replace('/', '-')));
        long time1 = cal.getTimeInMillis();
        cal.setTime(sdf.parse(endDate.replace('/', '-')));
        long time2 = cal.getTimeInMillis();
        long between_days = (time2 - time1) / (1000 * 3600 * 24);
        return Integer.parseInt(String.valueOf(between_days));
    }

    /**
     * [根据日期和天数间隔获取对应日期]<br>
     *
     * @param beginDate 开始日
     * @param days      间隔天数
     * @return 日期
     * @throws Exception 异常
     */
    public static String getDate(String beginDate, int days) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(sdf.parse(beginDate.replace('/', '-')));
        long time1 = cal.getTimeInMillis();
        long time2 = time1 + (long) days * 1000 * 3600 * 24;
        // 进行时间转换
        String date = sdf.format(new Date(time2));
        return date;
    }

    // 根据时间段，取其每周一的集合
    public static List<String> dealDiffDateWeek(String minDate, String maxDate) throws ParseException {
        ArrayList<String> result = new ArrayList<String>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");// 格式化为年月日
        Calendar min = Calendar.getInstance();
        min.setTime(sdf.parse(minDate));

        //判断起始日期为周一也增加到集合中
        if ((min.get(Calendar.DAY_OF_WEEK)) == 2) {
            result.add(sdf.format(min.getTime()));
        }

        //取时间段中的其他周一
        for (int i = 0; i < daysBetween(minDate, maxDate); i++) {
            min.add(Calendar.DAY_OF_MONTH, 1);
            if ((min.get(Calendar.DAY_OF_WEEK)) == 2) {
                result.add(sdf.format(min.getTime()));
            }
            if (maxDate.equals(sdf.format(min.getTime())))
                break;
        }
        return result;
    }

    public static List<String> getMonthBetween(String minDate, String maxDate) throws ParseException {
        ArrayList<String> result = new ArrayList<String>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");// 格式化为年月

        Calendar min = Calendar.getInstance();
        Calendar max = Calendar.getInstance();

        min.setTime(sdf.parse(minDate));
        min.set(min.get(Calendar.YEAR), min.get(Calendar.MONTH), 1);

        max.setTime(sdf.parse(maxDate));
        max.set(max.get(Calendar.YEAR), max.get(Calendar.MONTH), 2);

        Calendar curr = min;
        while (curr.before(max)) {
            result.add(sdf.format(curr.getTime()));
            curr.add(Calendar.MONTH, 1);
        }

        return result;
    }

    //日期转化为秒
    public static int getTimestamp(String datestr, String sf) {
        int unixTime = (int) (formatDate(datestr, sf).getTime() / 1000);
        if (unixTime < 0) {
            unixTime = Integer.MAX_VALUE;
        }
        return unixTime;
    }

    //秒转化为日期
    public static String getTimestampToDate(int datestr, String sf) {

        long millisecond = datestr * 1000L;

        return formatDate(new Date(millisecond), sf);
    }

    public static String get_SomeMonth(int month) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, month);
        return simpleDateFormat.format(calendar.getTime());
    }

    public static String get_SomeDayForMinute(int minute) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, minute);
        return simpleDateFormat.format(calendar.getTime());
    }

    public static String get_LastMonth() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
        Calendar cale = Calendar.getInstance();
        cale.set(Calendar.DAY_OF_MONTH, 0);
        return format.format(cale.getTime());
    }

    public static String get_last_day(int year, int month) {
        Calendar cal = Calendar.getInstance();
        //设置年份
        cal.set(Calendar.YEAR, year);
        //设置月份
        cal.set(Calendar.MONTH, month - 1);
        //获取某月最大天数
        int lastDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        //设置日历中月份的最大天数
        cal.set(Calendar.DAY_OF_MONTH, lastDay);
        //格式化日期
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String lastDayOfMonth = sdf.format(cal.getTime());

        return lastDayOfMonth;
    }

    public static int get_Month_Days(int year, int month) {
        Calendar a = Calendar.getInstance();
        a.set(Calendar.YEAR, year);
        a.set(Calendar.MONTH, month - 1);
        a.set(Calendar.DATE, 1);
        a.roll(Calendar.DATE, -1);
        int maxDate = a.get(Calendar.DATE);
        return maxDate;
    }

    public static String get_tomorrow(String day) {
        Calendar c = Calendar.getInstance();
        Date date = null;
        try {
            date = new SimpleDateFormat("yy-MM-dd").parse(day);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.setTime(date);
        int day1 = c.get(Calendar.DATE);
        c.set(Calendar.DATE, day1 + 1);

        String dayAfter = new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
        return dayAfter;
    }

    public static String getFirstDayOfNextMonth(String month) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        try {
            Date date = sdf.parse(month);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            calendar.add(Calendar.MONTH, 1);
            return sdf.format(calendar.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int getDaysByYearMonth(String date) {
        String year = date.substring(0, 4);
        String month = date.substring(5, 7);
        String day = date.substring(8, 10);
        Calendar a = Calendar.getInstance();
        a.set(Calendar.YEAR, Integer.parseInt(year));
        a.set(Calendar.MONTH, Integer.parseInt(month) - 1);
        a.set(Calendar.DATE, 1);
        a.roll(Calendar.DATE, -1);
        int maxDate = a.get(Calendar.DATE);
        int minDae = Integer.parseInt(day);
        int days = maxDate - minDae;
        return days;
    }

    public static String getPrevMonthDate(String dateTime, int n) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");//注意月份是MM
            Date date = simpleDateFormat.parse(dateTime);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - n);
            return new SimpleDateFormat("yyyy-MM").format(calendar.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getPreDateByDate(String strData) {
        String preDate = "";
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = sdf.parse(strData);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        c.setTime(date);
        int day1 = c.get(Calendar.DATE);
        c.set(Calendar.DATE, day1 + 1);
        preDate = sdf.format(c.getTime());
        return preDate;
    }

    /*获取当前月份的上一个月*/
    public static String getLastMonth() {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.MONTH, -1);
        Date m = c.getTime();
        String mon = formatDate(m, "yyyy-MM");
        return mon;
    }

    /**
     * 获取两个时间的时间差，精确到秒
     *
     * @param beginDate 开始日
     * @param endDate   结束日
     * @return 天数差
     * @throws ParseException
     * @throws Exception      异常
     */
    public static String dateBetween(String beginDate, String endDate) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.setTime(sdf.parse(beginDate.replace('/', '-')));
        long time1 = cal.getTimeInMillis();
        cal.setTime(sdf.parse(endDate.replace('/', '-')));
        long time2 = cal.getTimeInMillis();
        long between_days = (time2 - time1) / (1000 * 3600 * 24);
        long hour = (time2 - time1) / (60 * 60 * 1000) - (between_days * 24);
        long min = ((time2 - time1) / (60 * 1000) - between_days * 24 * 60 - hour * 60);
        long s = ((time2 - time1) / 1000 - between_days * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
        /*long ms = ((time2 - time1) - between_days * 24 * 60 * 60 * 1000 - hour * 60 * 60 * 1000
                - min * 60 * 1000 - s * 1000);*/
        StringBuilder timeDifference = new StringBuilder();
        if (between_days != 0) {
            timeDifference.append(between_days + "天");
        }
        if (hour != 0) {
            timeDifference.append(hour + "小时");
        }
        if (min != 0) {
            timeDifference.append(min + "分");
        }
        if (s != 0) {
            timeDifference.append(s + "秒");
        }
        return timeDifference.toString();
    }

    public static void main(String[] args) {
        try {
            System.out.println(dateBetween("2021-01-08 23:04:02", "2021-01-09 08:03:00"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
