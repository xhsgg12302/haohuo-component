package com.haohuo.utils;

import com.alibaba.fastjson.JSONObject;

import java.io.Serializable;


/**
 * Created by bianzhifu on 16/8/1.
 */
public class ComResult implements Serializable {

    private static final long serialVersionUID = 4520918576431487570L;
    /**
     * response result
     */
    private boolean result;
    /**
     * response code
     */
    private int retCode;
    /**
     * response message
     */
    private String retMsg;
    /**
     * response object
     */
    private Object retObj;


    /**
     * use on static in Shift.java
     * @param restStatus
     * @return this
     */
    public static ComResult build(RestStatus restStatus) {
        return new ComResult(true, restStatus.code(), restStatus.message(), null);
    }

    public static ComResult error(int code, String msg, Object obj) {
        return new ComResult(false, code, msg, obj);
    }

    public static ComResult error(int code, String msg) {
        return new ComResult(false, code, msg, null);
    }

    public static ComResult success() {
        return success(null);
    }

    public static ComResult success(Object obj) {
        return success(RetCode.RetOK_msg, obj);
    }

    public static ComResult success(String msg, Object obj) {
        return success(RetCode.RetOK, msg, obj);
    }

    public static ComResult success(int code, String msg, Object obj) {
        return new ComResult(true, code, msg, obj);
    }


    public ComResult(boolean result, int code, String strMsg, Object obj) {
        this.result = result;
        this.retCode = code;
        this.retMsg = strMsg;
        this.retObj = obj;
    }

    public ComResult(){

    }


    /**
     *  getter、setter and toString
     */
    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public int getRetCode() {
        return retCode;
    }

    public void setRetCode(int retCode) {
        this.retCode = retCode;
    }

    public String getRetMsg() {
        return retMsg;
    }

    public void setRetMsg(String retMsg) {
        this.retMsg = retMsg;
    }

    public Object getRetObj() {
        return retObj;
    }

    public void setRetObj(Object retObj) {
        this.retObj = retObj;
    }

    @Override
    public String toString() {
        JSONObject msgJson = new JSONObject();
        msgJson.put("result", result);
        msgJson.put("retCode", retCode);
        msgJson.put("retMsg", retMsg);
        msgJson.put("retObj", retObj);
        return msgJson.toJSONString();
    }
}
