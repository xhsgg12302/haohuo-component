package com.haohuo.utils;

/**
 * Created by bianzhifu on 16/8/1.
 */
public class RetCode {
    public static final int InsertFail = 100;
    public static final String InsertFail_msg = "插入失败";
    public static final int UpdateFail = 101;
    public static final String UpdateFail_msg = "更新失败";
    public static final int DelFail = 102;
    public static final String DelFail_msg = "删除失败";
    public static int RetOK = 0;
    public static String RetOK_msg = "正确执行";
    public static int RetNullPara = 80;
    public static String RetNullPara_msg = "缺少必要的参数";
    public static int RetJsonError = 81;
    public static String RetJsonError_msg = "json格式错误";
    public static int RetParmTypeError = 82;
    public static String RetParmTypeError_msg = "参数类型错误";
    public static int TOKENFAILED = 401;//token无效
    public static String TOKENMSG = "tokenInvalid!";
    public static int RetException = 99;
    public static String RetException_msg = "服务器端异常";
    /**服务商登录*/
    public static int VerlfyFail = 128;
    public static int RetError = 98;

    public static final int P_S_F_I_MYDYDDCZJ = 111;
    public static final String P_S_F_S_MYDYDDCZJ = "没有对应的组件或者组件没有显示指定service名称";
    public static final int P_S_F_I_WJLJBZQ = 112;
    public static final String P_S_F_S_WJLJBZQ = "文件路径不正确";

    public static final int P_S_F_I_RWYTJ = 113;
    public static final String P_S_F_S_RWYTJ = "任务已提交，结果查看任务详情";


    /**手动触发接口*/
    public static String TOKEN_MANUAL_MSG = "manual tokenInvalid!";

}
