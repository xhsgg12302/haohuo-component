package com.haohuo.utils;

/**
 * Created by HaoHuo on 2019/11/29.
 */
public interface RestStatus {

    int code();

    String message();
}
