package com.haohuo.utils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bianzhifu on 16/8/16.
 */
public class BeanMap<K, V> extends HashMap<K, V> implements Serializable {
    public Integer getIntegerValue(String key) {

        if (super.get(key) != null) {
            try {
                if (super.get(key) instanceof Long)
                    return ((Long) super.get(key)).intValue();
            }
            catch (Exception ex) {
                return null;
            }
            return (Integer) super.get(key);
        } else
            return null;
    }

    public Long getLongValue(String key) {
        if (super.get(key) != null){
            return Long.parseLong(super.get(key).toString());
        }
        return null;
    }

    public Double getDoubleValue(String key) {
        if (super.get(key) != null)
            return (Double) super.get(key);
        return null;
    }

    public BigDecimal getBigDecimalValue(String key) {
        if (super.get(key) != null)
            return (BigDecimal) super.get(key);
        else
            return null;
    }

    public String getStringValue(String key) {
        if (super.get(key) != null)
            return (String) super.get(key);
        return null;
    }

    public List getListValue(String key) {
        if (super.get(key) != null)
            return (List) super.get(key);
        return null;
    }

    public Map getMapValue(String key) {
        if (super.get(key) != null)
            return (Map) super.get(key);
        return null;
    }

    public Timestamp getTimestampValue(String key) {
        if (super.get(key) != null) {
            Timestamp time = (Timestamp) super.get(key);
            return time;
        } else
            return null;
    }

    public Date getDateValue(String key) {
        if (super.get(key) != null) {
            Date date = (Date) super.get(key);
            return date;
        }
        return null;
    }

    public String getDateValueToString(String key, String format) {
        if (super.get(key) != null) {
            Date date = getDateValue(key);
            return DateUtil.formatDate(date, format);
        }
        return null;
    }

    public String getTimestampValueToString(String key, String format) {
        Timestamp timestamp = getTimestampValue(key);
        if (timestamp != null)
            return DateUtil.formatDate(timestamp, format);
        else
            return "";
    }

    public String getObjectToString(String key) {
        Object o = super.get(key);
        if (o instanceof Integer) {
            return Integer.toString((Integer) o);
        } else if (o instanceof Long) {
            return Long.toString((Long) o);
        } else if (o instanceof Double) {
            return Double.toString((Double) o);
        } else if (o instanceof BigDecimal) {
            return o.toString();
        } else if (o instanceof Timestamp) {
            return getTimestampValueToString(key, DateUtil.FORMAT_DEFAULT);
        } else if (o instanceof Date) {
            return getDateValueToString(key, DateUtil.FORMAT_DEFAULT);
        } else if (o instanceof String) {
            return (String) o;
        } else {
            return "";
        }
    }
}
