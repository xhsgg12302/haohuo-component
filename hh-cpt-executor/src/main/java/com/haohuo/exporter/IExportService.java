package com.haohuo.exporter;

import com.alibaba.fastjson.JSONObject;
import com.haohuo.utils.ComResult;


public interface IExportService {
    /**
     * 增加导入任务
     */
    ComResult addTask(JSONObject obj);
    /**
     * 执行导入任务，回写任务记录表
     */
    ComResult excuteTask(int dtId);
}
