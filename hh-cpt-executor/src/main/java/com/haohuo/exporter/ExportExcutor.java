package com.haohuo.exporter;

import com.alibaba.fastjson.JSONObject;
import com.haohuo.utils.ComResult;
import com.haohuo.utils.RetCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

import static com.haohuo.utils.ComResult.error;
import static com.haohuo.utils.ComResult.success;


@Service
public class ExportExcutor{
    /** 导出组件列表**/
    @Autowired
    /* _12302_2021/6/11_< 直接在@Service中赋值，比如 @Service(value = "84" ) > */
    Map<String, AbstractExportService> exportServiceBaseMap;



    /*private static  Map<Integer, ExportServiceBase > workidAndBeannames= new HashMap<>();*/

    /*static {
        workidAndBeannames.put(61, "insuranceDismissalExport"); //企业减员记录导出
        workidAndBeannames.put(65, "claimsListExport"); //理赔统计报表导出
        workidAndBeannames.put(66, "claimsDetailListExport"); //理赔统计明细报表导出
        workidAndBeannames.put(67, "bussinessClaimsListExport"); //企业端我的理赔报表导出
        workidAndBeannames.put(68, "operationClaimsListExport"); //运营端我的理赔报表导出
        workidAndBeannames.put(69, "serviceClaimsListExport"); //服务商我的理赔报表导出
        workidAndBeannames.put(71, "downloadCompensateInfoExport"); //服务商我的理赔报表导出
        workidAndBeannames.put(75, "insuranceDismissalByOperExport"); //运营用户减员记录导出
        workidAndBeannames.put(83, "insuranceRecordListExport"); // 参保记录导出
        workidAndBeannames.put(84, "intelligenceOrderListExport"); //智能收保费导出

    }*/

    public ComResult excuteExport(JSONObject obj) {

        /** 根据 dtType 找到处理文件的实现类 **/
        Integer dtType = obj.getInteger("dtType");
        if(dtType==null)
            return error(-99, "必要参数缺失,dtType");

        AbstractExportService service = exportServiceBaseMap.get(dtType.toString())/*CptUtils.getService(exportServiceBaseMap,
                workidAndBeannames,dtType)*/;

        if(service == null)
            return error(RetCode.P_S_F_I_MYDYDDCZJ, RetCode.P_S_F_S_MYDYDDCZJ);

        /** 插入任务 **/
        ComResult ret = service.addTask(obj);
        if(ret != null && ret.getRetCode() != RetCode.RetOK)
            return ret;

        Integer dtId  = ((JSONObject)ret.getRetObj()).getInteger("flag");
        /** 重复提交 **/
        if(dtId==-2)
            return error(-99, "已有正在处理的上传操作，请稍后重试");

        /** 异步执行任务 **/
        service.excuteTask(dtId);

        ret.setRetMsg(RetCode.P_S_F_S_RWYTJ);
        return ret;
    }
}
