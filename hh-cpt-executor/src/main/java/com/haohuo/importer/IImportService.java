package com.haohuo.importer;

import com.alibaba.fastjson.JSONObject;
import com.haohuo.utils.ComResult;

public interface IImportService<DTO> {
    /**
     * 增加导入任务
     * @return
     */
    ComResult addTask(JSONObject obj);

    /**
     * 执行导入任务，回写任务记录表
     */
    ComResult excuteTask(int itId, Class<? extends DTO> pojoClasss);
}
