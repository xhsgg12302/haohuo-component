package com.haohuo.importer;

import com.alibaba.fastjson.JSONObject;
import com.haohuo.utils.ComResult;
import com.haohuo.utils.RetCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

import static com.haohuo.utils.ComResult.error;

@Service
public class ImportExcutor {
    /**
     * 导入组件列表
     **/
    @Autowired
    /* _12302_2021/6/11_< 直接在@Service中赋值，比如 @Service(value = "84" ) > */
    Map<String, AbstractImportService> importServices;

    /*private static  Map<Integer,ImportServiceBase> workidAndBeannames = new HashMap();*/

    /*static{
        workidAndBeannames.put(14,"ivPreCompImport");
        workidAndBeannames.put(15,"ivApplyTypeImport");
        workidAndBeannames.put(17,"ivPostInfoImport");

        workidAndBeannames.put(18,"ivMeasureImport");
    }*/

    public ComResult excuteImport(JSONObject obj, Class pojoClass) {

        /** 根据itType找到处理文件的实现类 **/
        Integer itType = obj.getInteger("itType");
        if (itType == null)
            return error(-99, "必要参数缺失,itType");

        AbstractImportService service = importServices.get(itType.toString());/*CptUtils.getService(importServices,
                workidAndBeannames,itType);*/

        if(service == null)
            return error(RetCode.P_S_F_I_MYDYDDCZJ, RetCode.P_S_F_S_MYDYDDCZJ);

        /** 插入任务 **/
        ComResult ret = service.addTask(obj);

        if (ret != null && ret.getRetCode() != RetCode.RetOK)
            return ret;

        Integer itId = ((JSONObject) ret.getRetObj()).getInteger("flag");

        /** 异步执行任务 **/
        service.excuteTask(itId, pojoClass);

        /* _12302_2021/6/7_< 即使检测不和格，也会返回成功。可以修改成，任务已提交，可查看任务详情。 > */
        ret.setRetMsg(RetCode.P_S_F_S_RWYTJ);
        return ret;
    }

}
