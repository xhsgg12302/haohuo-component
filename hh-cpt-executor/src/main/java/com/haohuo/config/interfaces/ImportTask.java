package com.haohuo.config.interfaces;

import java.io.Serializable;

public interface ImportTask extends Serializable {

    Integer getItId();

    ImportTask setItId(Integer itId);

    Integer getItType();

    ImportTask setItType(Integer itType);

    Integer getItOptId() ;

    ImportTask setItOptId(Integer itOptId);

    Integer getItUserId();

    ImportTask setItUserId(Integer itUserId);

    Integer getItState();

    ImportTask setItState(Integer itState);

    String getItStartTime();

    ImportTask setItStartTime(String itStartTime);

    String getItEndTime();

    ImportTask setItEndTime(String itEndTime);

    String getItParm();

    ImportTask setItParm(String itParm);

    String getItOssUrl();

    ImportTask setItOssUrl(String itOssUrl);

    String getItResultUrl();

    ImportTask setItResultUrl(String itResultUrl);

    String getItResult();

    ImportTask setItResult(String itResult);

    Integer getItPlat();

    ImportTask setItPlat(Integer itPlat);
    
}
