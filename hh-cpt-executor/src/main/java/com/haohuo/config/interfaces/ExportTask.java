package com.haohuo.config.interfaces;

import java.io.Serializable;

public interface ExportTask extends Serializable {

    Integer getDtId();

    ExportTask setDtId(Integer dtId);

    Integer getDtType();

    ExportTask setDtType(Integer dtType);

    Integer getDtOptId();

    ExportTask setDtOptId(Integer dtOptId);

    Integer getDtUserId();

    ExportTask setDtUserId(Integer dtUserId);

    Integer getDtState();

    ExportTask setDtState(Integer dtState);

    String getDtStartTime();

    ExportTask setDtStartTime(String dtStartTime);

    String getDtEndTime();

    ExportTask setDtEndTime(String dtEndTime);

    String getDtDes();

    ExportTask setDtDes(String dtDes);

    String getDtParm();

    ExportTask setDtParm(String dtParm);

    String getDtOssurl();

    ExportTask setDtOssurl(String dtOssurl);


}
