#!/bin/bash

read -p "请输入发布描述:" release_description

params="-Dmaven.test.skip=true -Dmaven.javadoc.skip=true -Dswagger2markup.skip=true -Dasciidoctor.skip=true "

mvn -B release:clean release:prepare -Darguments="${params}" -DscmReleaseCommitComment="${release_description}" release:perform -Darguments="${params}"
