package com.haohuo.framework.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * Copyright https://wtfu.site Inc. All Rights Reserved.
 * 
 * @date 2021/12/20
 *                          @since  1.0
 *                          @author 12302
 * 
 */

@RestController
@RequestMapping(value = "/demo")
public class DemoController {
    
    @GetMapping(value = "/{str}")
    public String demo(@PathVariable String str){
        return "hello " + str + "!";
    }
    
}
