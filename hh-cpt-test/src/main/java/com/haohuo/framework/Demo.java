package com.haohuo.framework;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * Copyright https://wtfu.site Inc. All Rights Reserved.
 * 
 * @date 2021/12/20
 *                          @since  1.0
 *                          @author 12302
 * 
 */
public class Demo {

    public static void main(String[] args) {
        createWindow();
    }

    private static void createWindow() {
        JFrame frame = new JFrame("Swing Tester");
        createUI(frame);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(460, 150);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    private static void createUI(final JFrame frame) {
        frame.getContentPane().setLayout(new BorderLayout(5,10));
        
        JPanel panel = new JPanel();
        frame.getContentPane().add(panel, BorderLayout.CENTER);
        
        panel.setLayout(new FlowLayout());

        JButton button = new JButton("退出");
        final JLabel label = new JLabel();
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String[] options = {"确定", "取消"};
                int result = JOptionPane.showOptionDialog(
                        frame,
                        "确定要退出？",
                        "Swing是/否提示信息~",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,     //no custom icon
                        options,  //button titles
                        options[0] //default button
                );
                if (result == JOptionPane.YES_OPTION) {
                    System.exit(0);
                } else if (result == JOptionPane.NO_OPTION) {
                    label.setText("选择：否");
                } else {
                    label.setText("未选择~");
                }
            }
        });

        panel.add(button);
        panel.add(label);
    }
    
}
