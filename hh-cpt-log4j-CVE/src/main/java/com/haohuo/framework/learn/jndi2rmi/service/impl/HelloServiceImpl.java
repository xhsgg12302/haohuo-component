package com.haohuo.framework.learn.jndi2rmi.service.impl;

import com.haohuo.framework.learn.jndi2rmi.service.IHello;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * Copyright https://wtfu.site Inc. All Rights Reserved.
 * 
 * @date 2021/12/19
 *                          @since  1.0
 *                          @author 12302
 * 
 */
public class HelloServiceImpl extends UnicastRemoteObject implements IHello {

    public HelloServiceImpl() throws RemoteException {
        super();
    }

    @Override
    public String sayHello(String name) throws RemoteException {
        return name;
    }
}
