package com.haohuo.framework.learn.jndi2rmi.rmi;

import com.haohuo.framework.learn.jndi2rmi.service.IHello;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 *
 * Copyright https://wtfu.site Inc. All Rights Reserved.
 * 
 * @date 2021/12/19
 *                          @since  1.0
 *                          @author 12302
 * 
 */
public class RmiClient {

    public static void main(String[] args) throws MalformedURLException, NotBoundException, RemoteException {
        IHello lookup = (IHello)Naming.lookup("rmi://127.0.0.1:1099/hello");
        System.out.println(lookup.sayHello("hello rmi"));
    }
    
}
