package com.haohuo.framework.learn.jndi2rmi.rmi;

import com.haohuo.framework.learn.jndi2rmi.service.IHello;
import com.haohuo.framework.learn.jndi2rmi.service.impl.HelloServiceImpl;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 *
 * Copyright https://wtfu.site Inc. All Rights Reserved.
 * 
 * @date 2021/12/19
 *                          @since  1.0
 *                          @author 12302
 * 
 */
public class RmiServer {

    public static void main(String[] args) throws RemoteException, AlreadyBoundException {
        // 创建对象
        IHello hello = new HelloServiceImpl();
        // 创建注册表
        Registry registry = LocateRegistry.createRegistry(1099);
        
        
        //Naming.bind("rmi://localhost:1234/testrmi", r);//需要带上端口
        registry.bind("hello", hello);
    }
}
