package com.haohuo.framework.learn.jndi2rmi.rmi;

import com.haohuo.framework.learn.jndi2rmi.service.IHello;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.rmi.RemoteException;
import java.util.Properties;

/**
 *
 * Copyright https://wtfu.site Inc. All Rights Reserved.
 * 
 * @date 2021/12/19
 *                          @since  1.0
 *                          @author 12302
 * 
 */
public class JNDIClient {

    public static void main(String[] args) throws NamingException, RemoteException {
        
        Properties env = new Properties();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.rmi.registry.RegistryContextFactory");
        env.put(Context.PROVIDER_URL, "rmi://127.0.0.1:1099");


        // 创建初始化环境
        Context ctx = new InitialContext(env);

        //JNDI动态协议转换
        IHello lookup = (IHello) ctx.lookup("rmi://127.0.0.1:1099/hello");
        System.out.println(lookup.sayHello("hello rmi"));
    }
    
}
