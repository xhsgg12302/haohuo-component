package com.haohuo.framework.learn.jndi2rmi.service;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * Copyright https://wtfu.site Inc. All Rights Reserved.
 * 
 * @date 2021/12/19
 *                          @since  1.0
 *                          @author 12302
 * 
 */
public interface IHello extends Remote {
    
    String sayHello(String name) throws RemoteException;
    
}
