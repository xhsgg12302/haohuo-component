/**
 * 
 * http；实现一个简单http服务器，用来下载 构造的文件，通过main 方法启动即可，有的使用的是 python，或者 nginx,apache 套件之类的
 * 
 * 
 * 
 * jndi: 包下有两种方式实现的目录命名服务，有rmi和ldap。对应Enter 中的 注入方式选择一个，启动即可
 *  
 *      注意： rmi 注入的时候给系统增加了一种属性，com.sun.jndi.rmi.object.trustURLCodebase = true,因为我当前JDK版本（1.8.0_181）
 *      默认是关闭（false）的，有些JDK版本是 打开的。
 *      
 *      
 * learn: 测试用一些例子，跟复现过程无关
 * 
 * 
 * payload: 用来构造展示的效果，或者攻击的逻辑。比如在目标及 弹出 计算器，或者对话框，再或者反弹shell之类
 * 
 * 
 * Enter.java 复现入口。
 * 
 *         
 *         
 *         
 * 复现过程：
 * 
 * 
 *     project(compiler) ---->  SimpleHttpServer（main)----->  LDAPRefServer(main)/RMIRefServer(main)---->  Enter(main)
 *     
 *     -----> 复现结果
 * 
 * 
 */
package com.haohuo.framework;