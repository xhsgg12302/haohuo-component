package com.haohuo.framework.http;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * Copyright https://wtfu.site Inc. All Rights Reserved.
 * 
 * @date 2021/12/17
 *                          @since  1.0
 *                          @author 12302
 * 
 */
public class SimpleHttpServer {

    public static void main(String[] args) {
        ServerSocket server=null;
        //线程池，简单来说，就是把线程start()改成ex.execute(),然后可以提高服务器性能
        ExecutorService ex= Executors.newFixedThreadPool(20);
        try {
            //建立服务器监听端口
            server=new ServerSocket(9080);
            System.out.println("start... 9080");
            while(true) {
                Socket s=server.accept();
                //然后把接收到socket传给SocketServer并执行该线程
                ex.execute(new SocketServer(s));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
}
